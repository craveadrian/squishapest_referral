<?php $this->suspensionRedirect($view); ?>
<!DOCTYPE html>
<html lang="en" <?php $this->helpers->htmlClasses(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<?php $this->helpers->seo($view); ?>
	<link rel="icon" href="public/images/favicon.png" type="image/x-icon">
	<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<link href="<?php echo URL; ?>public/styles/style.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	<link rel="stylesheet" href="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.css" media="screen" />
	<?php $this->helpers->analytics(); ?>
</head>

<body <?php $this->helpers->bodyClasses($view); ?>>
<?php $this->checkSuspensionHeader(); ?>
	<header>
		<div id="header">
			<div class="row">
				<div class="hdLeft col-8 fl">
					<nav>
						<a href="#" id="pull"><strong>MENU</strong></a>
						<ul>
							<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">HOME</a></li>
							<li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services#content">SERVICES</a></li>
							<li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery#content">GALLERY</a></li>
							<li <?php $this->helpers->isActiveMenu("revires"); ?>><a href="<?php echo URL ?>revires#content">REVIEWS</a></li>
							<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact#content">CONTACT US</a></li>
						</ul>
					</nav>
				</div>
				<div class="hdRight col-4 fr">
					<p>
						FOLLOW US
						<span>
							<a href="<?php $this->info("fb_link") ?>" class="socialico" target="_blank">F</a>
							<a href="<?php $this->info("tt_link") ?>" class="socialico" target="_blank">L</a>
							<a href="<?php $this->info("yt_link") ?>" class="socialico" target="_blank">X</a>
							<a href="<?php $this->info("rss_link") ?>" class="socialico" target="_blank">R</a>
						</span>
					</p>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</header>

	<?php if($view == "home"):?>
		<div id="banner">
			<div class="row">
				<h4>WHATS BUGGING YOU?</h4>
				<img src="public/images/common/bannerImg1.png" alt="Killer Bee" class="bee">
				<div class="bnrPanel">
					<div class="bnrPanelTop">
						<a href="<?php echo URL ?>"> <img src="public/images/common/mainLogo.png" alt="Squish a pest main logo" class="mainLogo"> </a>
					</div>
					<div class="bnrPanelLeft col-6 fl">
						<h2>LOCAL PEST CONTROL SERVICES</h2>
					</div>
					<div class="bnrPanelRight col-6 fl">
						<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip.</p>
					</div>
					<div class="clearfix"></div>
					<a href="<?php echo URL ?>services#content" class="btn">LEARN MORE</a>
				</div>
			</div>
		</div>
	<?php endif; ?>
