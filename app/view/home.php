<div id="content">
	<div id="wlcSection">
		<div class="row">
			<h5>WELCOME TO</h5>
			<h1>SQUISH A PEST</h1>
			<div class="wlcLeft col-3 fl">
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </p>
			</div>
			<div class="wlcMid col-6 fl">
				<img src="public/images/content/wlcImg1.png" alt="Rat">
			</div>
			<div class="wlcRight col-3 fl">
				<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
			</div>
			<div class="clearfix"></div>
			</div>
		</div>
	<div id="expSection">
		<div class="row">
			<h3>OVER 20 YEARS OF EXPERIENCE<span> IN THE BUSINESS.</span></h3>
			<div class="expLeft col-6 fl">
				<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
				<a href="<?php echo URL ?>service#content" class="btn">LEARN MORE</a>
				<a href="<?php echo URL ?>contact#content" class="btn2">CONTACT US</a>
			</div>
			<div class="expRight col-6 fl">
				<img src="public/images/content/expImg1.png" alt="rat">
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div id="expSection2">
		<div class="row">
			<div class="exp2Left col-8 fl">
				<div class="border">
					<div class="images">
						<img src="public/images/content/expImg1.jpg" alt="Rats">
						<img src="public/images/content/expImg2.jpg" alt="Ants">
						<img src="public/images/content/expImg3.jpg" alt="Cockroches">
					</div>
				</div>
			</div>
			<div class="exp2Right col-4 fl">
				<div class="exp2content">
					<h3>WHAT WE DO</h3>
					<ul>
						<li><p>Ants</p></li>
						<li><p>Bees</p></li>
						<li><p>Cockroaches</p></li>
						<li><p>Fleas</p></li>
						<li><p>Rodents</p></li>
						<li><p>Spiders</p></li>
						<li><p>Termites</p></li>
					</ul>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div id="cntDetSection">
		<div class="row">
			<h1>PROFESSIONAL EXTERMINATING WHATS BUGGING YOU?</h1>
			<h5>CALL US FOR DETAILS</h5>
			<div class="cntPanel">
				<div class="cntPanelTop">
					<div class="cntPanelLeft col-3 fl">
						<p>FOLLOW US</p>
						<p>
							<a href="<?php $this->info("fb_link"); ?>" target="_blank" class="socialico">f</a>
							<a href="<?php $this->info("tt_link"); ?>" target="_blank" class="socialico">l</a>
							<a href="<?php $this->info("yt_link"); ?>" target="_blank" class="socialico">x</a>
							<a href="<?php $this->info("rss_link"); ?>" target="_blank" class="socialico">r</a>
						</p>
					</div>
					<div class="cntPanelMid col-6 fl">
						<a href="<?php echo URL ?>"> <img src="public/images/common/mainLogo.png" alt="Logo"> </a>
					</div>
					<div class="cntPanelRight col-3 fl">
						<p>WE ACCEPT</p>
						<p>
							<img src="public/images/common/place.png" class="bg-card1" alt="sprite">
							<img src="public/images/common/place.png" class="bg-card2" alt="sprite">
						</p>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="cntPanelBottom">
					<div class="cntBottomLeft">
						<p> <img src="public/images/common/place.png" class="bg-location" alt="sprite"> <span><?php $this->info("address"); ?></span> </p>
					</div>
					<div class="cntBottomMid">
						<p> <img src="public/images/common/place.png" class="bg-phone" alt="sprite"> <span><?php $this->info(["phone","tel"]); ?></span> </p>
					</div>
					<div class="cntBottomRight">
						<p> <img src="public/images/common/place.png" class="bg-email" alt="sprite"> <span><?php $this->info(["email","mailto"]); ?></span> </p>
					</div>
				</div>
			</div>
			<img src="public/images/content/ant.png" alt="ant" class="ant resImg">
			<img src="public/images/content/spider.png" alt="spider" class="spider resImg">
		</div>
	</div>
</div>
